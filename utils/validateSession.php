session_start();

if(isset($_SESSION['userTypeId'])){
    if($_SESSION['userTypeId'] == 1)
        header("Location: indexAdmin.php");
    if($_SESSION['userTypeId'] == 2)
        header("Location: indexHelpDesk.php");
    if($_SESSION['userTypeId'] >= 3 && $_SESSION['userTypeId'] <= 7)
        header("Location: indexUser.php");
}
