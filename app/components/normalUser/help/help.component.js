angular.module('help').component('help', {
   templateUrl: 'app/components/normalUser/help/help.template.html',
   controller: function HelpController($scope, $http) {
       $.ajax({
           url: 'http://'+_host+':8000/Session',
           async: false,
           type: 'get',
           dataType: 'json',
           xhrFields: {
               withCredentials: true
           },
           success: function (response) {
               $scope.usr = response;
               console.log($scope.usr);
           },
           error: function (error) {
               console.log(error);
           }
       });


       $http.get('http://'+_host+':8000/Conversation/User/' + $scope.usr.userrId)
           .then(function (response) {
               $scope.conversation = response.data;
               location.href = '#!Chat/'+$scope.conversation.conversationId;
           }, function (error) {
               alert("an herror has occurred");
               console.log(error);
           });

       this.$onInit = function () {

        };

        this.$onDestroy = function () {

        };

        angular.element(document).ready(function () {

        });
   }
});
