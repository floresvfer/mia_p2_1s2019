angular.module('productList').component('productList', {
    templateUrl: 'app/components/normalUser/product-list/product-list.template.html',
    controller: [
        '$scope',
        '$http',
        '$routeParams',
        function ProductListController($scope, $http, $routeParams) {
            let categoryId = $routeParams.categoryId;

            $.ajax({
                url: 'http://'+_host+':8000/Session',
                async: false,
                type: 'get',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                success: function (response) {
                    $scope.usr = response;
                    console.log($scope.usr);
                },
                error: function (error) {
                    console.log(error);
                }
            });

            if(categoryId === undefined) {
                $http.get('http://' + _host + ':8000/Product/All')
                    .then(function (response) {
                        $scope.products = response.data;
                    }, function (error) {
                        alert("an herror has occurred");
                        console.log(error);
                    });
            }else{
                $http.get('http://' + _host + ':8000/Product/All/'+categoryId)
                    .then(function (response) {
                        $scope.products = response.data;
                    }, function (error) {
                        alert("an herror has occurred");
                        console.log(error);
                    });

            }

            $scope.showBtn =  (id) => {return $scope.usr.userrId === id};

            $scope.deleteProduct = (id) => {
                if (confirm('Do you really wanna delete the product')){
                    $http.delete('http://'+_host+':8000/Product/' + id)
                        .then(function (response) {
                            location.reload();
                        }, function (error) {
                            alert("an error has occurred");
                            console.log(error);
                        });
                }
            };

            $scope.updateProduct = (id) => {
                alert('update '+id);
                location.href = "#!/shop";
            };
        }
    ]
});
