angular.module('chat').component('chat', {
    templateUrl: 'app/components/normalUser/chat/chat.template.html',
    controller: [
        '$scope',
        '$http',
        '$routeParams',
        function ChatController($scope, $http, $routeParams) {
            $scope.conversationId = $routeParams.conversationId;
            $http.get('http://'+_host+':8000/Conversation/'+$scope.conversationId)
                .then(function (response) {
                    $scope.conversation = response.data;
                    if(angular.equals($scope.conversation, {}))
                        location.href = 'index.html';
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });

            $http.get('http://'+_host+':8000/Message/0')
                .then(function (response) {
                    $scope.message = response.data;
                    $scope.message.conversationId = parseInt($scope.conversationId);
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });

            $.ajax({
                url: 'http://'+_host+':8000/Session',
                async: false,
                type: 'get',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                success: function (response) {
                    $scope.usr = response;
                    console.log($scope.usr);
                },
                error: function (error) {
                    console.log(error);
                }
            });

            $http.get('http://'+_host+':8000/Message/All/' + $scope.conversationId)
                .then(function (response) {
                    $scope.messages = response.data;
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });

            $scope.getCommentClass = function (usrComment, usrSession) {
                if (usrComment === usrSession)
                    return 'row my-comment comment';
                return 'row ex-comment comment';
            };

            $scope.getCommentClassLeft = function (usrComment, usrSession) {
                if (usrComment === usrSession)
                    return 'col-md-6 rt';
                return 'col-md-1';
            };

            $scope.getImageClass = function (usrComment, usrSession) {
                if (usrComment === usrSession)
                    return 'my-comment-img';
                return 'ex-comment-img';
            };

            $scope.getNumber = function (num) {
                console.log(num);
                return new Array(num);
            };

            let scrollBottomChat = () => {
                let objDiv = document.getElementById('messagesDiv');
                objDiv.scrollTop = objDiv.scrollHeight;
            };

            let getMessagesService = () => {
                $http.get('http://'+_host+':8000/Message/All/' + $scope.conversationId)
                    .then(function (response) {
                        $scope.messagesTmp = response.data;
                        if ($scope.messagesTmp.length !== $scope.messages.length) {
                            $scope.messages = $scope.messagesTmp;
                            scrollBottomChat();
                            scrollBottomChat();
                            scrollBottomChat();
                        }
                    }, function (error) {
                        alert("an herror has occurred");
                        console.log(error);
                    });
            };

            function PlaySound() {
                let sound = document.getElementById("boom");
                sound.play();
            }

            $scope.sendValoration = function(){
                $scope.conversation.valoration = parseInt($('#valoration option:selected').val());
                console.log($scope.conversation);
                $http.put(
                    'http://'+_host+':8000/Conversation',
                    JSON.stringify($scope.conversation),
                    {
                        headers: {'Content-Type': 'application/json'}
                    })
                    .then(function (response) {
                        location.href = 'index.html';
                    }, function (error) {
                        alert("an herror has occurred");
                        console.log(error);
                    });
            };

            $scope.sendComment = function () {
                $scope.message.senddate = new Date(Date.now());
                $scope.message.senddate = $scope.message.senddate.toISOString().slice(0, 19).replace('T', ' ');
                $scope.message.userId = $scope.usr.userrId;
                $scope.messages.push($scope.message);
                $http.post(
                    'http://'+_host+':8000/Message',
                    JSON.stringify($scope.message),
                    {
                        headers: {'Content-Type': 'application/json'}
                    })
                    .then(function (response) {
                        $http.get('http://'+_host+':8000/Message/All/' + $scope.conversationId)
                            .then(function (response) {
                                $scope.messages = response.data;
                                getMessagesService();
                            }, function (error) {
                                alert("an herror has occurred");
                                console.log(error);
                            });
                    }, function (error) {
                        alert("an herror has occurred");
                        console.log(error);
                    });

            };

            angular.element(document).ready(function () {
                scrollBottomChat();
            });

            this.$onInit = function () {
                this.id =
                    setInterval(getMessagesService, 2500);
            };

            this.$onDestroy = function () {
                if (this.id)
                    clearInterval(this.id);
            }
        }
    ]
});
