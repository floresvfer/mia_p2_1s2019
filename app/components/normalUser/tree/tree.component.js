angular.module('tree').component('tree', {
    templateUrl: 'app/components/normalUser/tree/tree.template.html',
    controller: [
        '$scope',
        '$http',
        '$routeParams',
        function ChatController($scope, $http, $routeParams) {
            $scope.setId = (categoryId) => { return 'childs_'+categoryId; };
            $scope.showChilds = function (categoryId) {
                let _this = $('#father_' + categoryId);
                let _childs = $('#childs_'+categoryId);

                let clas = _this.attr('class');
                if(clas === 'one ng-scope') {
                    _this.attr('class', 'two ng-scope');
                    _childs.show();
                }else{
                    _this.attr('class', 'one ng-scope');
                    _childs.hide();
                }
            };

            $scope.goTo = function (categoryId) {
                location.href = '#!/shop/'+categoryId;
            };

            function getTree(categories){
                let tree = categories.filter((category) => {
                    return category.categoryFatherId === -1;
                });
                tree.map((category) => {
                    category.childs = getTreeChild(categories, category.categoryId);
                });
                return tree;
            }

            function getTreeChild(categories, fatherId) {
                let childs = categories.filter((category) => {
                    return category.categoryFatherId === fatherId
                });
                childs.map((category) => {
                    category.childs = getTreeChild(categories, category.categoryId);
                });
                return childs;
            }

            $http.get('http://'+_host+':8000/Category/Tree')
                .then(function (response) {
                    $scope.categories = getTree(response.data);
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });
        }
    ]
});
