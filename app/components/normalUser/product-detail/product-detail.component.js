angular.module('productDetail').component('productDetail', {
    templateUrl: 'app/components/normalUser/product-detail/product-detail.template.html',
    controller: [
        '$scope',
        '$http',
        '$routeParams',
        function ProductDetailController($scope, $http, $routeParams) {
            let productId = $routeParams.productId;
            $http.get('http://'+_host+':8000/Comment/0')
                .then(function (response) {
                    $scope.comment = response.data;
                    $scope.comment.productId = parseInt(productId);
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });



                $.ajax({
                    url: 'http://'+_host+':8000/Session',
                    async: false,
                    type: 'get',
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (response) {
                        $scope.usr = response;
                        console.log($scope.usr);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });


            $http.get('http://'+_host+':8000/Product/' + productId)
                .then(function (response) {
                    $scope.product = response.data;
                    console.log($scope.product);
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });

            $http.get('http://'+_host+':8000/Comment/All/' + productId)
                .then(function (response) {
                    $scope.comments = response.data;
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });

            $scope.getCommentClass = function (usrComment, usrSession) {
                if (usrComment === usrSession)
                    return 'row my-comment comment';
                return 'row ex-comment comment';
            };

            $scope.getCommentClassLeft = function (usrComment, usrSession) {
                if (usrComment === usrSession)
                    return 'col-md-4 rt';
                return 'col-md-1';
            };

            $scope.getImageClass = function (usrComment, usrSession) {
                if (usrComment === usrSession)
                    return 'my-comment-img';
                return 'ex-comment-img';
            };

            $scope.getNumber = function(num) {
                console.log(num);
                return new Array(num);
            };

            $scope.addToCart = function(){
                alert(parseInt($('#quantity option:selected').val()));
                alert($scope.product.productId);
            };

            $scope.sendComment = function () {
                $scope.comment.publishdate = new Date(Date.now());
                $scope.comment.publishdate =
                    $scope.comment.publishdate.toISOString().slice(0, 19).replace('T', ' ');
                $scope.comment.userId = $scope.usr.userrId;
                $scope.comments.push($scope.comment);
                $scope.comment.valoration = parseInt($('#valoration option:selected').val());
                $http.post(
                    'http://'+_host+':8000/Comment',
                    JSON.stringify($scope.comment),
                    {
                        headers: {'Content-Type': 'application/json'}
                    })
                    .then(function (response) {
                        $http.get('http://'+_host+':8000/Comment/All/' + productId)
                            .then(function (response) {
                                $scope.comments = response.data;
                            }, function (error) {
                                alert("an herror has occurred");
                                console.log(error);
                            });
                    }, function (error) {
                        alert("an herror has occurred");
                        console.log(error);
                    });


            };

            angular.element(document).ready(function () {
                $('#etalage').etalage({
                    thumb_image_width: 300,
                    thumb_image_height: 300,

                    show_hint: true,
                    click_callback: function (image_anchor, instance_id) {
                        alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');
                    },
                    change_callback: function () {
                        alert('it moved');
                    }
                });
            });
        }
    ]
});
