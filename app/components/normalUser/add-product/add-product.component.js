angular.module('addProduct').component('addProduct', {
    templateUrl: 'app/components/normalUser/add-product/add-product.template.html',
    controller: [
        '$scope',
        '$http',
        '$routeParams',
        function AddProductController($scope, $http, $routeParams) {
            $scope.product = {
                "productId": 1,
                "name": "",
                "code": "Y89YY3RRR",
                "category": {
                    "categoryId": 1,
                    "categoryFather": 2,
                    "description": "categoria",
                    "status": 0
                },
                "user": {
                    "userId": 1,
                    "userType": {
                        "userTypeId": 0,
                        "description": "admin",
                        "status": 0
                    },
                    "gender": {
                        "genderId": 0,
                        "description": "Masculino",
                        "status": 0
                    },
                    "name": "fer",
                    "lastname": "flores",
                    "password": "pass",
                    "mail": "mymail@gmail.com",
                    "phone": "54252919",
                    "photo": "/home/photos/photo.jpg",
                    "birthdate": "2018-01-25",
                    "registerdate": "2018-01-25",
                    "address": "29 av, 26-12 Z7",
                    "availableCredit": 5000,
                    "gainObtained": 400,
                    "validateCode": "f4MtvVAfXO2nnZZCeEnTGDpImEJ5MqVA",
                    "status": 0
                },
                "photo": "photo.png",
                "description": "",
                "price": 50.74,
                "publisdate": "2019-04-19",
                "amount": 10,
                "status": 0
            };

            $http.get('http://'+_host+':8000/Category/Tree')
                .then(function (response) {
                    $scope.categories = response.data;
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });

            $.ajax({
                url: 'http://'+_host+':8000/Session',
                async: false,
                type: 'get',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                success: function (response) {
                    $scope.usr = response;
                    console.log($scope.usr);
                },
                error: function (error) {
                    console.log(error);
                }
            });

            $scope.sendProduct = function () {
                $scope.product.publisdate = new Date(Date.now());
                $scope.product.publisdate = $scope.product.publisdate.toISOString().slice(0, 19).replace('T', ' ');
                $scope.product.user.userId = $scope.usr.userrId;
                $scope.product.amount = parseInt($scope.product.amount);
                $scope.product.price = parseFloat($scope.product.price);
                $scope.product.category.categoryId = parseInt($scope.product.category.categoryId);
                $http.post(
                    'http://'+_host+':8000/Product',
                    JSON.stringify($scope.product),
                    {
                        headers: {'Content-Type': 'application/json'}
                    })
                    .then(function (response) {
                        location.href = '#!/shop'
                    }, function (error) {
                        alert("an herror has occurred");
                        console.log(error);
                    });

            };
        }
    ]
});
