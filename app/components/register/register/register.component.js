angular.module('register').component('register', {
    templateUrl: 'app/components/register/register/register.template.html',
    controller: function RegisterController($scope, $http) {
        $http.get('http://'+_host+':8000/User/0')
            .then(function (response) {
                response.data.birthdate = new Date(response.data.birthdate);
                response.data.registerdate = new Date(Date.now());
                $scope.master = response.data;
                $scope.user = response.data;
            }, function (error) {
                alert("an herror has occurred");
                console.log(error);
            });

        $http.get('http://'+_host+':8000/Gender/All/0')
            .then(function (response) {
                $scope.genders = response.data;
            }, function (error) {
                alert("an herror has occurred");
                console.log(error);
            });


        $scope.send = function () {
            $scope.user.birthdate = $scope.user.birthdate.toISOString().slice(0, 19).replace('T', ' ');
            $scope.user.registerdate = $scope.user.registerdate.toISOString().slice(0, 19).replace('T', ' ');
            $scope.user.gender.genderId = parseInt($scope.user.gender.genderId, 10);
            $http.post(
                'http://'+_host+':8000/User',
                JSON.stringify($scope.user),
                {
                    headers: {'Content-Type': 'application/json'}
                })
                .then(function (response) {
                    alert('Ok');
                    console.log(response);
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });
            $scope.user.birthdate = new Date($scope.user.birthdate);
            $scope.user.registerdate = new Date(Date.now());
        };

        $scope.reset = function () {
            $scope.user = angular.copy($scope.master);
        };

        $scope.reset();

        angular.element(document).ready(function () {
            $('#reset').click();
        });



    }
});
