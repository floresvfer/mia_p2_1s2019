angular.module('userList').component('userList', {
    templateUrl: 'app/components/adminUser/user-list/user-list.template.html',
    controller: [
        '$scope',
        '$timeout',
        '$http',
        '$routeParams',
        function UserListController($scope, $timeout, $http, $routeParams) {
            $http.get('http://'+_host+':8000/User/All')
                .then(function (response) {
                    $scope.users = response.data;
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });

            $scope.EditUser = (id) => {
                location.href = 'update.html#!/Users/' + id;
            };

            $scope.DeleteUser = function (id) {
                if (confirm('Do you really wanna delete user: ' + id)) {
                    $http.delete('http://'+_host+':8000/User/' + id)
                        .then(function (response) {
                            location.reload();
                        }, function (error) {
                            alert("an error has occurred");
                            console.log(error);
                        });
                }
            };

            $scope.getClass = function(status){
                if(status === 0)
                    return 'actived';
                if(status === 1)
                    return 'unconfirmed';
                if(status === 2)
                    return 'deleted';
            };

            angular.element(document).ready(function () {

            });

            $timeout(function () {
                    let dTable = $('#user_table');
                    dTable.DataTable({
                        responsive: true
                    });
            }, 1000);
        }

    ]
});
