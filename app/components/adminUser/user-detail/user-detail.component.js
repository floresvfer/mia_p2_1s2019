angular.module('userDetail').component('userDetail', {
    templateUrl: 'app/components/adminUser/user-detail/user-detail.template.html',
    controller: [
        '$scope',
        '$http',
        '$routeParams',
        function UserDetailController($scope, $http, $routeParams) {
            let userId = $routeParams.userId;
            if(userId === undefined){
                $.ajax({
                    url: 'http://'+_host+':8000/Session',
                    async: false,
                    type: 'get',
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (response) {
                        $scope.usr = response;
                        userId = $scope.usr.userrId;
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }


            $http.get('http://'+_host+':8000/User/'+userId)
                .then(function (response) {
                    response.data.birthdate = new Date(response.data.birthdate);
                    response.data.registerdate = new Date(Date.now());
                    $scope.master = response.data;
                    $scope.user = response.data;
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });

            $http.get('http://'+_host+':8000/Gender/All/0')
                .then(function (response) {
                    $scope.genders = response.data;
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });


            $scope.send = function () {
                $scope.user.birthdate = $scope.user.birthdate.toISOString().slice(0, 19).replace('T', ' ');
                $scope.user.registerdate = $scope.user.registerdate.toISOString().slice(0, 19).replace('T', ' ');
                $scope.user.gender.genderId = parseInt($scope.user.gender.genderId, 10);
                //alert($scope.user.gender.genderId);
                $http.put(
                    'http://'+_host+':8000/User',
                    JSON.stringify($scope.user),
                    {
                        headers: {'Content-Type': 'application/json'}
                    })
                    .then(function (response) {
                        location.reload();
                        console.log(response);
                    }, function (error) {
                        alert("an herror has occurred");
                        console.log(error);
                    });
                $scope.user.birthdate = new Date($scope.user.birthdate);
                $scope.user.registerdate = new Date(Date.now());
            };

            $scope.reset = function () {
                $scope.user = angular.copy($scope.master);
                $('#gender option:selected').val($scope.user.userId);
            };

        }
    ]
});
