angular.module('reportTable').component('reportTable', {
    templateUrl: 'app/components/adminUser/report-table/report-table.template.html',
    controller: [
        '$scope',
        '$timeout',
        '$http',
        '$routeParams',
        function ReportTableController($scope, $timeout, $http, $routeParams) {
            $scope.reportId = $routeParams.reportId;
            $scope.x = $routeParams.x;
            $scope.header = getHeaders($routeParams.reportId);
            $scope.column = getColumns($routeParams.reportId);

            function getHeaders(reportId) {
                reportId = parseInt(reportId);
                switch (reportId) {
                    case 1:
                        return '<th>No</th><th>Help Desk</th><th>Punctuation</th>';
                    case 2:
                        return '<th>No</th><th>Help Desk</th><th>Sex</th><th>Birthdate</th>';
                    case 3:
                        return '<th>No</th><th>Admin</th><th>Sex</th><th>Birthdate</th>';
                    case 4:
                        return '<th>No</th><th>Client</th><th>Gains</th>';
                    case 5:
                        return '<th>No</th><th>Product</th><th>Punctuation</th>';
                    case 7:
                        return '<th>No</th><th>User</th><th>Products Amount</th>';
                    case 8:
                        return '<th>No</th><th>Client</th><th>Products Amount</th>';
                    case 9:
                        return '<th>No</th><th>Product</th><th>Comments</th><th>Publish Date</th>';
                    case 10:
                        return '<th>No</th><th>Product</th><th>Amount</th>';
                    case 11:
                        return '<th>No</th><th>Product</th><th>Punctuation</th>';
                    default:
                        return '';
                }
            }

            function getColumns(reportId) {
                reportId = parseInt(reportId);
                let res = '';
                let rows = [];
                $.ajax({
                    url: 'http://'+_host+':8000/Report2/'+reportId+'/'+$scope.x+'/y',
                    async: false,
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        rows = response;
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                switch (reportId) {
                    case 1:
                        for(let i = 0; i<rows.length; i++)
                            res += '<tr><td>'+(i+1)+'</td><td>'+rows[i].mail+'</td><td>'+rows[i].average+'</td></tr>';
                        return res;
                    case 2:
                        for(let i = 0; i<rows.length; i++)
                            res += '<tr><td>'+(i+1)+'</td><td>'+rows[i].mail+'</td><td>Masculino</td><td>'+rows[i].birthdate+'</td></tr>';
                        return res;
                    case 3:
                        for(let i = 0; i<rows.length; i++)
                            res += '<tr><td>'+(i+1)+'</td><td>'+rows[i].mail+'</td><td>Femenino</td><td>'+rows[i].birthdate+'</td></tr>';
                        return res;
                    case 4:
                        for(let i = 0; i<rows.length; i++)
                            res += '<tr><td>'+(i+1)+'</td><td>'+rows[i].mail+'</td><td>'+rows[i].gain+'</td></tr>';
                        return res;
                    case 5:
                        for(let i = 0; i<rows.length; i++)
                            res += '<tr><td>'+(i+1)+'</td><td>'+rows[i].name+'</td><td>'+rows[i].valoration+'</td></tr>';
                        return res;
                    case 7:
                        for(let i = 0; i<rows.length; i++)
                            res += '<tr><td>'+(i+1)+'</td><td>'+rows[i].mail+'</td><td>'+rows[i].cantidad+'</td></tr>';
                        return res;
                    case 8:
                        return '';
                    case 9:
                        for(let i = 0; i<rows.length; i++)
                            res += '<tr><td>'+(i+1)+'</td><td>'+rows[i].name+'</td><td>'+rows[i].comentarios+'</td><td>date</td></tr>';
                        return res;
                    case 10:
                        for(let i = 0; i<rows.length; i++)
                            res += '<tr><td>'+(i+1)+'</td><td>'+rows[i].mail+'</td><td>'+rows[i].amount+'</td></tr>';
                        return res;
                    case 11:
                        for(let i = 0; i<rows.length; i++)
                            res += '<tr><td>'+(i+1)+'</td><td>'+rows[i].name+'</td><td>'+rows[i].valoration+'</td></tr>';
                        return res;
                    default:
                        return '';
                }
            }


            angular.element(document).ready(function () {
                $('#_header').html($scope.header);
                $('#_columns').html($scope.column);
            });

            $timeout(function () {
                let dTable = $('#report_table');
                dTable.DataTable({
                    responsive: true
                });
            },);
        }
    ]
});
