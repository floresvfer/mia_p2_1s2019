angular.module('reportList').component('reportList', {
    templateUrl: 'app/components/adminUser/report-list/report-list.template.html',
    controller: [
        '$scope',
        '$http',
        '$routeParams',
        '$timeout',
        function ReportListController($scope, $http, $routeParams, $timeout) {
            $scope.reports = [];
            for(let i = 1; i<=11; i++)
                $scope.reports.push({
                    no: i,
                    name: 'Report '+i,
                });

            $.ajax({
                url: 'http://'+_host+':8000/Session',
                async: false,
                type: 'get',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                success: function (response) {
                    $scope.usr = response;
                    console.log($scope.usr);
                },
                error: function (error) {
                    console.log(error);
                }
            });

            $scope.GoToPdf = (id) =>{
                let x = $('#x_y_'+id).val();
                x = x === "" ? "x": x;
                window.open('http://'+_host+':8000/Report/'+id+'/'+x+'/y');
            };

            $scope.GoToInApp = (id) => {
                let x = $('#x_y_'+id).val();
                x = x === "" ? "x": x;
                location.href = "#!/Report/"+id+"/"+x;
            };

            $timeout(function () {
                let dTable = $('#reports_table');
                dTable.DataTable({
                    responsive: true
                });
            }, 1000);
        }
    ]
});
