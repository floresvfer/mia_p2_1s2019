angular.module('chatList').component('chatList', {
    templateUrl: 'app/components/adminUser/chat-list/chat-list.template.html',
    controller: [
        '$scope',
        '$http',
        '$routeParams',
        '$timeout',
        function ChatListController($scope, $http, $routeParams, $timeout) {
            $.ajax({
                url: 'http://'+_host+':8000/Session',
                async: false,
                type: 'get',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                success: function (response) {
                    $scope.usr = response;
                    console.log($scope.usr);
                },
                error: function (error) {
                    console.log(error);
                }
            });

            $http.get('http://'+_host+':8000/Conversation/All/'+$scope.usr.userrId)
                .then(function (response) {
                    $scope.conversations = response.data;
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });

            $scope.GoToConversation = (id) => {
                location.href = '#!/Chat/'+id;
            };

            angular.element(document).ready(function () {

            });

            $timeout(function () {
                let dTable = $('#conversations_table');
                dTable.DataTable({
                    responsive: true
                });
            }, 1000);
        }
    ]
});
