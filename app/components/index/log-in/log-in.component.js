angular.module('logIn').component('logIn', {
    templateUrl: 'app/components/index/log-in/log-in.template.html',
    controller: [
        '$scope',
        '$http',
        '$routeParams',
        function LogIn($scope, $http, $routeParams) {
            $scope.mail = '';
            $scope.password = '';
            $scope.text = '';

            angular.element(document).ready(function () {

            });

            $scope.logIn = function () {
                $http.get('http://'+_host+':8000/User/LogIn/' + $scope.mail + '/' + $scope.password)
                    .then(function (response) {
                        let user = response.data;
                        if(user.length === 0){
                            $scope.text = 'Please verify your mail, password or your verification mail sent to your email';
                            return;
                        }
                        user = user[0];
                        $scope.text = '';

                        $http.get(
                            'http://'+_host+':8000/Session/'+user.userrId+
                            '/'+user.userTypeId+
                            '/'+user.genderId+
                            '/'+'blahblah'+
                            '/'+user.name+
                            '/'+user.mail,
                            {withCredentials : true}
                        )
                            .then(function (response) {
                                if(user.userTypeId === 1)
                                    location.href = 'indexAdmin.html';
                                if(user.userTypeId === 2)
                                    location.href = 'indexHelpDesk.html';
                                if(user.userTypeId >= 3 && user.userTypeId <= 7)
                                    location.href = 'indexUser.html';
                            }, function (error) {
                                alert('an error has occurred');
                            });
                    });

            };

            $scope.clear = function () {
                $scope.text = '';
            };
        }
    ]
});
