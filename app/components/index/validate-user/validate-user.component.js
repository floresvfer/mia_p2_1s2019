angular.module('validateUser').component('validateUser', {
    templateUrl: 'app/components/index/validate-user/validate-user.template.html',
    controller: [
        '$scope',
        '$http',
        '$routeParams',
        function UserValidateController($scope, $http, $routeParams) {
            $http.get('http://'+_host+':8000/User/validate/'+$routeParams.validateCode)
                .then(function (response) {
                    if(response.data.status === 0) {
                        alert('Invalid link, please contact with the admin');
                        location.href = "index.php";
                    }
                }, function (error) {
                    alert("an herror has occurred");
                    console.log(error);
                });

            angular.element(document).ready(function () {

            });
        }
    ]
});
