angular.module('indexApp', [
    'ngRoute',
    'home',
    'validateUser',
    'logIn'
]);

angular.module('registerApp', [
    'ngRoute',
    'register'
]);

angular.module('updateUserApp', [
    'ngRoute',
    'userDetail'
]);

angular.module('normalUserApp', [
    'ngRoute',
    'home',
    'productDetail',
    'productList',
    'help',
    'chat',
    'tree',
    'addProduct'
]);

angular.module('adminUserApp', [
    'ngRoute',
    'home',
    'userList',
    'chat',
    'chatList',
    'reportList',
    'reportTable'
]);

angular.module('helpDeskUserApp', [
    'ngRoute',
    'home',
    'chat',
    'chatList'
]);
