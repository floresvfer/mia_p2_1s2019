angular.module('indexApp').config(['$routeProvider',
    function config($routeProvider) {
        $routeProvider.when('/', {
            template: '<home></home>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                }
            }
        }).when('/validate/:validateCode', {
            template: '<validate-user></validate-user>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                }
            }
        }).when('/logIn', {
            template: '<log-in></log-in>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });

                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                }
            }
        }).otherwise('/');
    }
])
;

angular.module('registerApp').config(['$routeProvider',
    function config($routeProvider) {
        $routeProvider.when('/', {
            template: '<register></register>'
        }).otherwise('/');
    }
]);

angular.module('updateUserApp').config(['$routeProvider',
    function config($routeProvider) {
        $routeProvider.when('/Users/:userId', {
            template: '<user-detail></user-detail>'
        }).when('/myAccount', {
            template: '<user-detail></user-detail>'
        }).otherwise('/');
    }
]);

angular.module('normalUserApp').config(['$routeProvider',
    function config($routeProvider) {
        $routeProvider.when('/', {
            template: '<home></home>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/shop', {
            template: '<product-list></product-list>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/shop/:categoryId', {
            template: '<product-list></product-list>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/product/:productId', {
            template: '<product-detail></product-detail>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/addProduct', {
            template: '<add-product></add-product>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/Help', {
            template: '<help></help>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/Chat/:conversationId', {
            template: '<chat></chat>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/Categories', {
            template: '<tree></tree>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).otherwise('/');
    }
])
;

angular.module('adminUserApp').config(['$routeProvider',
    function config($routeProvider) {
        $routeProvider.when('/', {
            template: '<home></home>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/Users', {
            template: '<user-list></user-list>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/Chat/:conversationId', {
            template: '<chat></chat>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/Chats', {
            template: '<chat-list></chat-list>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/Reports', {
            template: '<report-list></report-list>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/Report/:reportId/:x', {
            template: '<report-table></report-table>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 2)
                        location.href = 'indexHelpDesk.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).otherwise('/');
    }
]);

angular.module('helpDeskUserApp').config(['$routeProvider',
    function config($routeProvider) {
        $routeProvider.when('/', {
            template: '<home></home>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/Chat/:conversationId', {
            template: '<chat></chat>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).when('/Chats', {
            template: '<chat-list></chat-list>',
            resolve: {
                "check": function () {
                    let sess = [];
                    $.ajax({
                        url: 'http://'+_host+':8000/Session',
                        async: false,
                        type: 'get',
                        dataType: 'json',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (response) {
                            sess = response;
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    if (sess.userTypeId === 1)
                        location.href = 'indexAdmin.html';
                    if (sess.userTypeId >= 3 && sess.userTypeId <= 7)
                        location.href = 'indexUser.html';
                    if(sess.userTypeId === undefined)
                        location.href = 'index.html';
                }
            }
        }).otherwise('/');
    }
]);


