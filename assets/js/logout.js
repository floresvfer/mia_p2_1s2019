$.ajax({
    url: 'http://'+_host+':8000/Session',
    async: false,
    type: 'delete',
    xhrFields: {
        withCredentials: true
    },
    success: function (response) {
        location.href = 'index.html#!/';
    },
    error: function (error) {
        alert('an error has occurred');
        location.href = 'index.html#!/';
    }
});

